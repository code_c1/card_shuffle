#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SUITS 4
#define FACES 13
#define CARD 52

typedef struct card {
    char *suit;
    char *face;
} Card;


void fillDeck(Card * const d, char *s[], char *f[]);
void shuffle(Card * const d);
void deal(Card *const d);


int main(void) {

    Card deck[CARD];

    char *suit[SUITS] = {"Hearts", "Diamonds", "Clubs", "Spades"};
    char *face[FACES] = {"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};

    srand(time(NULL));

    fillDeck(deck, suit, face);

    shuffle(deck);
    deal(deck);

    return 0;
}

void fillDeck(Card * const d, char *s[], char *f[]) {
    size_t i;
    for (i = 0; i < CARD; i++) {
        d[i].face = f[i % FACES];
        d[i].suit = s[i / FACES];
    }
}

void shuffle(Card *const d) {
    size_t i;
    size_t j;
    Card temp;

    for(i = 1; i < CARD; i++) {
        j = rand() % CARD;
        temp = d[i];
        d[i] = d[j];
        d[j] = temp;
    }
}

void deal(Card *const d) {
    size_t i;
    for(i = 0; i < CARD; i++){
        (i + 1) % 4 == 0 ? printf("%2ld. %6s of %-10s\n", i + 1 , d[i].face, d[i].suit) : printf("%2ld. %6s of %-10s", i, d[i].face, d[i].suit);
    }


}
    
